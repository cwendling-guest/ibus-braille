#!/usr/bin/python3

import gi
gi.require_version('IBus', '1.0')

from gi.repository import IBus
from gi.repository import GLib
from gi.repository import GObject

from Xlib.keysymdef.latin1 import *
from Xlib.keysymdef.miscellany import *

import http.client
import json
import re
import brlapi
from espeak import espeak

try:
    from Xlib.keysymdef.braille import *

except:
    XK_braille_dot_1 = 0xfff1
    XK_braille_dot_8 = 0xfff8
    XK_braille_dot_9 = 0xfff9
    XK_braille_dot_10 = 0xfffa
    XK_braille_blank = 0x1002800

dot9_mask  = 1 << ( 9 - 1)
dot10_mask = 1 << (10 - 1)

import time

import louis

espeak.set_voice("en")
def say(msg):
    espeak.synth(msg)

# Interval during which all keys should be released at the same time
release_delay = 0.3

contracted_louis_tables = [ 'en-us-brf.dis', 'unicode.dis', 'fr-bfu-g2.ctb' ]
literal_louis_tables = [ 'en-us-brf.dis', 'unicode.dis', 'fr-bfu-comp8.utb' ]

# TODO: configurable
std_map = {
    30: 7, # q
    31: 3, # s
    32: 2, # d
    33: 1, # f
    36: 4, # j
    37: 5, # k
    38: 6, # l
    39: 8, # ;
}

brf = {
    '⠀': ' ',
    '⠁': 'a',
    '⠃': 'b',
    '⠉': 'c',
    '⠙': 'd',
    '⠑': 'e',
    '⠋': 'f',
    '⠛': 'g',
    '⠓': 'h',
    '⠊': 'i',
    '⠚': 'j',
    '⠅': 'k',
    '⠇': 'l',
    '⠍': 'm',
    '⠝': 'n',
    '⠕': 'o',
    '⠏': 'p',
    '⠟': 'q',
    '⠗': 'r',
    '⠎': 's',
    '⠞': 't',
    '⠥': 'u',
    '⠧': 'v',
    '⠺': 'w',
    '⠭': 'x',
    '⠽': 'y',
    '⠵': 'z',
    '⡁': 'A',
    '⡃': 'B',
    '⡉': 'C',
    '⡙': 'D',
    '⡑': 'E',
    '⡋': 'F',
    '⡛': 'G',
    '⡓': 'H',
    '⡊': 'I',
    '⡚': 'J',
    '⡅': 'K',
    '⡇': 'L',
    '⡍': 'M',
    '⡝': 'N',
    '⡕': 'O',
    '⡏': 'P',
    '⡟': 'Q',
    '⡗': 'R',
    '⡎': 'S',
    '⡞': 'T',
    '⡥': 'U',
    '⡧': 'V',
    '⡺': 'W',
    '⡭': 'X',
    '⡽': 'Y',
    '⡵': 'Z',
    '⠡': '1',
    '⠣': '2',
    '⠩': '3',
    '⠹': '4',
    '⠱': '5',
    '⠫': '6',
    '⠻': '7',
    '⠳': '8',
    '⠪': '9',
    '⠼': '0',
}

backbrf = {
    a : b for b, a in brf.items()
}

def to_brf(s):
    s2 = ""
    for c in s:
        if c in brf:
            c = brf[c]
        s2 = s2 + c
    return s2

def from_brf(s):
    s2 = ""
    for c in s:
        if c in backbrf:
            c = backbrf[c]
        s2 = s2 + c
    return s2

# TODO: needs to be user-defined
separators = {
    '⠀': ' ',
    '⠂': ',',
    '⠲': '.',
    '⠒': ':',
    '⠆': ';',
}

level = 5
def debug(l, *args):
    if l > level:
        return
    print(*args)

class EngineBraille(IBus.Engine):
    __gtype_name__ = 'EngineBraille'

    # whether PC Braille typing is enabled
    PC_enabled = False

    # whether Braille uncontraction is enabled
    uncontraction = False

    # whether Math mode is enabled (no liblouis uncontraction, not BRF display)
    math = False

    # keyboard mapping for dots
    key_map = std_map

    # mask of dots being pressed
    pressed = 0

    # mask of dots being committed
    committing = 0

    # when the user started to release keys
    release_start = 0

    # current word
    preedit = ""

    # whether the current word is a contracted word being typed or a transcribed text
    contracted = False

    # brlapi.Connection object
    brlapi_connection = None

    # whether last key event was a release
    releasing = False

    def __init__(self):
        super(EngineBraille, self).__init__()
        debug(5, "init")

        self.__is_invalidate = False
        self.__preedit_string = ""
        self.brlapi_init()

    def do_enable(self):
        # Tell ibus we will use surrounding text
        self.get_surrounding_text()
        self.do_focus_in(self)

    def do_focus_out(self):
        debug(2, "focus out")
        self.__commit()
        self.PC_enabled = False
        self.uncontraction = False
        self.brlapi_set()

    def do_process_key_event(self, keysym, scancode, state):
        """
        A key was pressed or released
        """

        text = self.get_surrounding_text()
        s = text.text.get_text()
        debug(4, "text is '%s'" % s)

        is_press = (state & IBus.ModifierType.RELEASE_MASK) == 0

        has_control = (state & IBus.ModifierType.CONTROL_MASK) != 0
        has_alt = (state & IBus.ModifierType.MOD1_MASK) != 0
        has_shift = (state & IBus.ModifierType.SHIFT_MASK) != 0
        has_super = (state & IBus.ModifierType.SUPER_MASK) != 0

        was_releasing = self.releasing
        self.releasing = is_press == False

        def say_mode():
            if self.math:
                debug(2, "math mode")
                say("Maths mode")
            elif self.uncontraction:
                debug(2, "contracted text")
                say("Contracted mode")
            else:
                debug(2, "literal text")
                say("Literal mode")

        if ((keysym == XK_Shift_L or keysym == XK_Shift_R) and has_control and has_alt) or \
           ((keysym == XK_Control_L or keysym == XK_Control_R) and has_shift and has_alt) or \
           ((keysym == XK_Alt_L or keysym == XK_Alt_R or keysym == XK_Meta_L or keysym == XK_Meta_R) and has_control and has_shift):
            if not is_press and not was_releasing:
                self.PC_enabled = not self.PC_enabled
                if self.PC_enabled:
                    debug(2, "PC enabled")
                    say_mode()
                else:
                    debug(2, "PC disabled")
                    say("Braille disabled")
            return True

        if ((keysym == XK_Shift_L or keysym == XK_Shift_R) and has_control) or \
           ((keysym == XK_Control_L or keysym == XK_Control_R) and has_shift):
            if not is_press and not was_releasing:
                if keysym == XK_Shift_R or keysym == XK_Control_R:
                    if not self.math:
                        self.math = True
                        self.uncontraction = False
                    else:
                        self.math = False
                else:
                    if not self.uncontraction:
                        self.math = False
                        self.uncontraction = True
                    else:
                        self.uncontraction = False
                self.brlapi_set()
                if not self.uncontraction:
                    self.__commit()
                say_mode()
            return True

        if keysym == XK_m and has_control:
            if is_press:
                debug(2, "switching maths")
                text = self.get_surrounding_text()
                s = text.text.get_text()
                slen = len(s)
                c = text.cursor_pos
                a = text.anchor_pos

                pos = s.find('\n')
                if pos != -1:
                    s = s[:pos]

                debug(2, "text is '%s'" % s)
                debug(4, "cursor is %s" % c)
                debug(4, "anchor is %s" % a)

                # Translate with natbraille's server
                n = ord(s[0])
                if n >= 0x2800 and n <= 0x28FF:
                    #debug(2, "from Braille to MathML")
                    #url = "mathbraille2mathml.json"
                    debug(2, "from Braille to StarMath")
                    url = "mathbraille2starmath.json"
                    s = from_brf(s).replace(chr(0x2800),'')
                else:
                    #debug(2, "from MathML to Braille")
                    #url = "mathml2braille.json"
                    debug(2, "from StarMath to MathML to Braille")
                    url = "starofficeToMathMLToBraille.json"

                s2 = s
                s2 = s2.replace('"', '\\"')
                #s2 = s2.replace('\n', '\\n')
                #s2 = s2.replace('\t', '\\t')
                s2 = s2.replace('\n', ' ')
                s2 = s2.replace('\t', ' ')

                debug(2, "text sent to natbraille is '%s'" % s2)

                conn = http.client.HTTPConnection("localhost:4567")
                conn.request("POST", "/tdf/" + url,
                        b'{ ' +
                            b'"string" : "' + s2.encode("utf-8") + b'", ' +
                            b'"charset" : "UTF-8", ' +
                            b'"contentType" : "text/plain", ' +
                            b'"brailleTable" : "TbFr2007", ' +
                            b'"filename" : "myfilename", ' +
                        b'}',
                        {"Content-Type":"application/json"})
                r = conn.getresponse()
                d = r.read()
                conn.close()
                v = json.loads(d.decode("utf-8"))
                debug(4, "natbraille status '" + v['status'] + "'")
                #XXX if v['status'] == 'OK':
                if 'document' in v:
                    self.delete_surrounding_text(-c, slen)

                    s = v["document"]["string"]

                    # XXX
                    i = s.find("<?xml")
                    if i != -1:
                        i = s.find("?>", i)
                        s = s[i+2:]

                    i = s.find("<starmath")
                    if i != -1:
                        i = s.find(">", i)
                        s = s[i+1:]

                    i = s.rfind("</starmath>")
                    if i != -1:
                        s = s[:i]

                    i = s.find("<body")
                    if i != -1:
                        i = s.find(">", i)
                        s = s[i+1:]

                    i = s.rfind("</body>")
                    if i != -1:
                        s = s[:i]

                    i = s.find("<p")
                    if i != -1:
                        i = s.find(">", i)
                        s = s[i+1:]

                    i = s.rfind("</p>")
                    if i != -1:
                        s = s[:i]

                    # XXX
                    s = s.replace("<m:", "<")
                    s = s.replace("</m:", "</")

                    debug(2, "text was '%s'" % s)
                    while True:
                        i = s.find("<span")
                        if i == -1:
                            break
                        j = s.find(">", i)
                        s = s[:i] + s[j+1:]

                    while True:
                        i = s.find("</span")
                        if i == -1:
                            break
                        j = s.find(">", i)
                        s = s[:i] + s[j+1:]

                    # XXX
                    s = re.sub(" +", " ", s)
                    s = s.replace("\n", "")

                    # XXX
                    s = s.strip()
                    s = s.rstrip()

                    if url == "starofficeToMathMLToBraille.json":
                        s = chr(0x2800) + to_brf(s)

                    c = len(s)
                    #c = 2

                    debug(2, "text is now '%s'" % s)
                    self.commit_text(IBus.Text.new_from_string(s))
                    for i in range(len(s) - c):
                        self.forward_key_event(XK_Left, 105, 0)
            return True

        # keysym is AT scancode,
        debug(4, "got key %x %d %x" % (keysym, scancode, state))

        if keysym & ~0xff == XK_braille_blank:
            self.__process_pattern(keysym & 0xff)
            return True
        elif keysym >= XK_braille_dot_1 and keysym <= XK_braille_dot_10:
            self.__process_dot_key(keysym - XK_braille_dot_1 + 1, is_press)
            return True

        if self.PC_enabled and scancode in self.key_map:
            dot = self.key_map[scancode]
            self.__process_dot_key(dot, is_press)
            return True

        self.__commit()
        # don't eat
        return False


    def brlapi_init(self):
        """Initialization, called only once at startup"""

        def _brlapi_init():
            """Try to (re)connect to BrlAPI"""
            self.brlapi_connection = brlapi.Connection()
            self.brlapi_connection.enterTtyModeWithPath()
            self.brlapi_set()
            GLib.io_add_watch(self.brlapi_connection.fileDescriptor, GLib.IOCondition.IN, process_brlapi)

        def _brlapi_reconnect():
            """Callback which tries to reconnect to BrlAPI, to be called at
            regular interval"""
            try:
                _brlapi_init()
                debug(4, "Reconnected to BrlAPI")
                return False
            except:
                return True

        def process_brlapi(fd, cond):
            """The BrlAPI fd is available for read, process it"""
            try:
                k = self.brlapi_connection.readKey(wait = False)
            except:
                debug(4, "BrlAPI connection lost, trying to reconnect...")
                try:
                    self.brlapi_connection.closeConnection()
                except:
                    pass
                self.brlapi_connection = None
                GLib.timeout_add_seconds(1, _brlapi_reconnect)
                return False

            if k == None:
                # Processed some data, but no key event in the pipe
                return True
            dots = k & brlapi.KEY_CMD_ARG_MASK
            debug(4, "got braille %x from BrlAPI" % dots)
            self.__process_pattern(dots)
            return True

        """At initialization, only try to connect to BrlAPI once, and if not
        available, assume that it will never be"""
        try:
            _brlapi_init()
        except:
            debug(4, "BrlAPI not available")
            pass

    def brlapi_set(self):
        """Update whether BrlAPI should pass us the dot patterns, or let
        something else (e.g. brltty+xbrlapi) process them"""
        if self.brlapi_connection == None:
            # Not connected
            return

        if self.uncontraction or self.math:
            self.brlapi_connection.acceptKeyRanges([(brlapi.KEY_TYPE_CMD|brlapi.KEY_CMD_PASSDOTS, brlapi.KEY_TYPE_CMD|brlapi.KEY_CMD_PASSDOTS|brlapi.KEY_CMD_ARG_MASK)])
        else:
            self.brlapi_connection.ignoreAllKeys()



    def __process_dot_key(self, dot, is_press):
        """
        A braille key was pressed or released
        """
        debug(4, "got key " + str(dot))
        dot_mask = 1 << (dot - 1)
        if is_press:
            self.pressed |= dot_mask
        else:
            if self.committing == 0 or time.time() - self.release_start > release_delay:
                # starting committing, or the release_delay was overcome, so
                self.committing = self.pressed
                debug(4, "start committing " + str(self.committing))
                self.release_start = time.time()
            self.pressed &= ~dot_mask
            if self.pressed == 0 and self.committing != 0:
                # committed a braille pattern, record it
                pattern = self.committing
                self.committing = 0
                self.__process_pattern(pattern)

    def __process_pattern(self, pattern):
        """
        A braille pattern was committed
        """
        # TODO: support half-layouts
        extra = pattern & (dot9_mask | dot10_mask)
        if extra == dot9_mask:
            # with space key
            if pattern == dot9_mask:
                self.__process_char('⠀')
            # TODO: others
            else:
                debug(1, "unsupported pattern 0x%x" % pattern)
        elif extra == dot10_mask:
            debug(1, "unsupported pattern 0x%x" % pattern)
        elif extra == dot9_mask | dot10_mask:
            debug(1, "unsupported pattern 0x%x" % pattern)
        else:
            char = chr(0x2800 | pattern)
            self.__process_char(char)

    def __process_char(self, char):
        debug(3, "got char " + char)
        if self.math:
            # For sighted users, show characters instead of braille
            # patterns for a-z
            self.commit_text(IBus.Text.new_from_string(to_brf(char)))
        elif char in separators:
            # A separator got pressed, commit word
            self.__commit()
            # And commit the separator
            self.commit_text(IBus.Text.new_from_string(separators[char]))
        elif not self.uncontraction:
            # Convert character immediately
            self.__process_word(from_brf(char))
        else:
            # Keep it in preedit for now
            self.preedit += to_brf(char)
            self.contracted = True
            debug(4, "preedit now " + self.preedit)
            # But show it as brf for now too
            self.update_preedit_text_with_mode(IBus.Text.new_from_string(self.preedit), len(self.preedit), True, 1)

    def __commit(self):
        """
        A separator was pressed, commit the braille word if any
        """
        if self.preedit == "":
            return
        word = self.preedit
        self.preedit = ""
        debug(3, "committing " + word)
        self.update_preedit_text_with_mode(IBus.Text.new_from_string(""), 0, False, 0)
        if self.math:
            # Prepend a braille blank to make sure we know it is braille for converting to starmath
            self.commit_text(IBus.Text.new_from_string(chr(0x2800)+to_brf(word)))
        else:
            self.__process_word(from_brf(word))
        self.contracted = False

    def __process_word(self, word):
        """
        A braille word should be untranscribed and emitted
        """
        debug(2, "running on '" + word + "'")
        if self.contracted:
            debug(2, "with contration")
            word = louis.backTranslate(contracted_louis_tables, word)[0]
        else:
            word = louis.backTranslate(literal_louis_tables, word)[0]
        debug(2, "got '" + str(word) + "'")
        self.commit_text(IBus.Text.new_from_string(word))


class IMApp:
    def __init__(self, exec_by_ibus):
        self.__component = IBus.Component.new("org.freedesktop.IBus.Braille",
                                    "Braille2 Component", "0.1", "BSD",
                                    "Samuel Thibault <samuel.thibault@ens-lyon.org",
                                    "http://hypra.fr/",
                                    "/bin/true", "braille2")
        engine = IBus.EngineDesc.new("braille2", "Braille2", "Braille2", "",
                                    "BSD", "Samuel Thibault <samuel.thibault@ens-lyon.org",
                                    "", "")

        self.__component.add_engine(engine)
        self.__mainloop = GLib.MainLoop()
        self.__bus = IBus.Bus()
        self.__bus.connect("disconnected", self.__bus_disconnected_cb)
        self.__factory = IBus.Factory.new(self.__bus.get_connection())
        self.__factory.add_engine("braille2", GObject.type_from_name("EngineBraille"))

        if exec_by_ibus:
            self.__bus.request_name("org.freedesktop.Ibus.Braille", 0)
        else:
            self.__bus.register_component(self.__component)
            self.__bus.set_global_engine_async("braille2", -1, None, None, None)

    def run(self):
        self.__mainloop.run()

    def __bus_disconnected_cb(self, bus):
        self.__mainloop.quit()


def main():
    exec_by_ibus = False
    IBus.init()
    IMApp(exec_by_ibus).run()

if __name__ == "__main__":
    main()
